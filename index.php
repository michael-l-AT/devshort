<?php

// This file is part of the devShort project under the MIT License. Visit https://github.com/flokX/devShort for more information.

$config_path = implode(DIRECTORY_SEPARATOR, array(__DIR__, "data", "config.json"));
$config_content = json_decode(file_get_contents($config_path), true);
$stats_path = implode(DIRECTORY_SEPARATOR, array(__DIR__, "data", "stats.json"));
$stats_content = json_decode(file_get_contents($stats_path), true);

// Count the access
$statsfile = fopen($stats_path, 'c+');
if (flock($statsfile, LOCK_EX))
{
    $stats_content = json_decode(fread($statsfile, filesize($stats_path)), true);
    $stats_content["index"][date("Y-m-d")] += 1;

    ftruncate($statsfile, 0);
    fseek($statsfile, 0);
    fwrite($statsfile, json_encode($stats_content, JSON_PRETTY_PRINT));
    flock($statsfile, LOCK_UN);
}
fclose($statsfile);

// Generate custom buttons for the footer
$links_string = "";
if ($config_content["settings"]["custom_links"]) {
    foreach ($config_content["settings"]["custom_links"] as $name => $url) {
        $links_string = $links_string . "<a href=\"$url\" class=\"badge badge-primary\" target=\"_blank\">$name</a> ";
    }
    $links_string = substr($links_string, 0, -1);
}

$author_string = "";
if ($config_content["settings"]["author_link"]) {
    $author_string = "<a rel=\"me\" target=\"_blank\" href=\"". $config_content["settings"]["author_link"] ."\">".$config_content["settings"]["author"]."</a>";
} else {
    $author_string = $config_content["settings"]["author"];
}

function show_random_shortlink() {
	global $config_content;

	if (!$config_content["settings"]["random_shortlink"]) { return; }
	
	$random_short = array_rand($config_content["shortlinks"]);
	$random_url = dirname($_SERVER['PHP_SELF']) . $random_short; 
	echo "<h2>Explore</h2><p class=\"lead\">We've choosen a random shortlink for you: <a href=\"$random_url\">$random_short</a></p>";
}

?>

<!doctype html>
<html class="h-100" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex, nofollow">
    <meta name="author" content="<?php echo $config_content["settings"]["author"]; ?> and the devShort team">
    <link href="<?php echo $config_content["settings"]["favicon"]; ?>" rel="icon">
    <title><?php echo $config_content["settings"]["name"]; ?></title>
    <link href="assets/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">

    <main class="flex-shrink-0">
        <div class="container">
            <nav class="mt-3" aria-label="breadcrumb">
                <ol class="breadcrumb shadow-sm">
                    <li class="breadcrumb-item"><a href="<?php echo $config_content["settings"]["home_link"]; ?>">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?php echo $config_content["settings"]["name"]; ?></li>
                </ol>
            </nav>
            <h1 class="mt-5"><?php echo $config_content["settings"]["name"]; ?></h1>
            <p class="lead">This is a shortlink service. You need a valid shortlink to get redirected.</p>
            <?php show_random_shortlink(); ?>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="https://en.wikipedia.org/wiki/URL_shortening" target="_blank">What is URL shortening?</a></li>
                <li class="list-inline-item">-</li>
                <li class="list-inline-item"><a href="<?php echo $config_content["settings"]["home_link"]; ?>">Home page</a></li>
                <li class="list-inline-item">-</li>
                <li class="list-inline-item"><a href="admin.php">Admin panel</a></li>
            </ul>
        </div>
    </main>

    <footer class="footer mt-auto py-3 bg-dark">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <span class="text-light">&copy; 2020 <?php echo $author_string; ?> and <a href="https://src.clttr.info/rwa/devshort" target="_blank">devShort</a></span>
                <?php if ($links_string) { echo "<span class=\"text-muted\">$links_string</span>"; } ?>
            </div>
        </div>
    </footer>

</body>

</html>
