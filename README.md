# devShort

The devShort URL shortener is the perfect choice for web developers, admins and professionals. It is simple and privacy friendly but very powerful since it focuses on the most important things.

### Jump to our [demo](https://clttr.info)!

## 1-2-3 Main features
1. Slim but powerful
    * No database required
    * Fast installation
    * Integrated statistics viewer
    * No feature overhead
2. Widely configurable for your needs with only one file
3. Privacy friendly
    * Only the access count and date is tracked
    * The whole code is verifiable in minutes
    * Privacy and security by default

### modifications compared to original devShort
* Short-URLs are case-insensitive, means "link" and "LINK" are the same redirect
* Explore section: show a random shortlink on index page
* fix for loosing counters

## 1-2-3 Reasons to install
1. Give everyone a good-looking and easy to remember link
2. Know when and how many times the links are used
3. Have no hassle with the software because it's slim and easy to configure
4. Absolutely no external deps, all that's required is shipped with the software.

## 1-2-3 Step installation
1. Download the latest [release](https://src.clttr.info/rwa/devshort/releases) and upload it to the desired installation place
2. Move `admin/config.json.example` to `admin/config.json` and `admin/stats.json.example` to `admin/stats.json`
3. Adjust the configuration in `admin/config.json`, especially **insert a admin password**   
The passwort will be securely hashed at the first login on the admin page.
4. Start using devShort! 

### Requirements
* Webserver (Apache, nginx, Caddy, ...)
* PHP 7.4 or above

## 1-2-3 Step update
1. Download the latest [release](https://src.clttr.info/rwa/devshort/releases) and upload it to the installation place
2. Start using devShort!

## Configuration
All configuration is done in the file `data/config.json`. Below is a list of values that should be adjusted by the instance admin.

* `admin_password` -> password for the admin panel, is securely hashed after first login
* `name` -> name of your site
* `author` -> name of the author/admin
* `author_link` -> link to your profile on a social network, keybase or similar, uses the "rel=me" flag
* `home_link` -> link to your homepage
* `favicon` -> relative path to your favicon, e.g. `assets/icon.png`
* `random_shortlink` -> show "explore" section on index page, `true` or `false`
* `custom_links` -> arbitrary amount of custom links that are shown on the right side of the footer

## About
* Maintainer: [René Wagner](https://src.clttr.info/rwa)
* Creator: [flokX](https://github.com/flokX)

License: [The MIT License](https://git.sr.ht/~rwa/devshort/tree/master/LICENSE)

Third-party credits:
* [Vue.js](https://vuejs.org/)
* [Bootstrap](https://getbootstrap.com)
* [Frappe Charts](https://github.com/frappe/charts)
* [IO-Images](https://pixabay.com/users/io-images-1096650) 
